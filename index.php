<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[Company Name] - Prototype Design</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,600,800,700' rel='stylesheet' type='text/css'>
<link href="css/styles.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	
	// Copyright Notice
	
    $('#copyright i').on('click', function() { 
		$('.overlay').fadeOut(500);
		$('#copyright').addClass('fadeOutUp');
	});
	
	// Device View Buttons
	
	$('.buttons a').on('click', function() {
		var target = $(this).data('target');		
		if (!$(target).is(':visible')) {
			$('.view').stop(true).animate({'opacity': 0},500, function() {
				$(this).hide();
				$(target).show().animate({'opacity':1});
			});
		}
	});		
});

</script>
</head>
<body>
	<div class="overlay">
		<div id="copyright" class="animated"> <img src="images/crearelogo.png" />
    		<p>[Company Name] prototype design &copy; Creare Communications Limited 2014 </p>
    		<i class="fa fa-times"></i>
		</div>
	</div>
	<div class="buttons">
    	<img src="images/crearelogo.png" />
		<ul>
			<li><a href="javascript:void(0);" data-target="#desktop"><i class="fa fa-desktop"></i><span>Desktop</span></a></li>
    		<li><a href="javascript:void(0);" data-target="#mobile"><i class="fa fa-mobile"></i><span>Mobile</span></a></li>
    		<li><a href="javascript:void(0);" data-target="#category"><i class="fa fa-th-list"></i><span>Category Page</span></a></li>
    		<li><a href="javascript:void(0);" data-target="#cat-mob"><i class="fa fa-mobile"></i><span>Category Mobile</span></a></li>
    		<li><a href="javascript:void(0);" data-target="#product"><i class="fa fa-gift"></i><span>Product View</span></a></li>
            <li><a href="javascript:void(0);" data-target="#prod-mob"><i class="fa fa-mobile"></i><span>Product Mobile</span></a></li>
    	</ul>
	</div>
	<div class="view first" id="desktop"> <img class="hp" src="images/desktop.jpg" /> </div>
	<div class="view" id="mobile"> <img src="images/mobile.jpg" /> </div>
	<div class="view" id="category"> <img class="hp" src="images/category.jpg" /> </div>
	<div class="view" id="cat-mob"> <img src="images/category-mobile.jpg" /> </div>
	<div class="view" id="product"> <img class="hp" src="images/product.jpg" /> </div>
    <div class="view" id="prod-mob"> <img src="images/product-mobile.jpg" /> </div>
</body>
</html>
