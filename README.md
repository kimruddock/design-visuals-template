======= Design Visuals Template =======

A template system for selecting and displaying multiple design visuals on the same page.

** Features **

- Optional copyright notice
- Ability to add any number of visuals
- Font-Awesome sourced directly from CDN
 
** Basic Use **

- Download the blank template folder. 
- Unzip and upload the contents to the remote root folder for your prototype.
- The blank template contains 6 design views with corresponding images. The simplest way to use the visuals template is to replace these image files with your own and keep the file names the same.
- If you don't need all 6 views just delete or comment out the unwanted <a> tags from the 'buttons' div and then the corresponding 'view' divs.

** Creating Your Own Buttons and Views **

- If the basic button setup doesn't match your needs then copy and paste to create a new list item in the 'buttons' div
- Change the data attribute to the id of your new view i.e. data-target="#mynewview"
- Add a suitable icon or text to describe your new view
- Creare your new 'view' div with the id you just set on the data attribute i.e. <div class="view" id="mynewview"></div>
- If you want your new view to appear first add a class of 'first' to the div (be sure to remove any other instances of 'first' )

** Adding HTML **

- You can add HTML instead of just an image to any 'view' div. Add any relevant CSS at the bottom of the styles.css stylesheet

Any questions or bugs found please drop me an email kim.ruddock@creare.co.uk
